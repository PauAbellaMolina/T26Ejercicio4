package es.http.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class T26Ejercicio4Application {

	public static void main(String[] args) {
		SpringApplication.run(T26Ejercicio4Application.class, args);
	}

}
