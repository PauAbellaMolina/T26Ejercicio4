package es.http.service.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.http.service.dto.Equipo;
import es.http.service.service.EquipoServiceImpl;

@RestController
@RequestMapping("/api")
public class EquipoController {

	@Autowired
	EquipoServiceImpl equipoServiceImpl;
	
	@GetMapping("/equipos")
	public List<Equipo> listarEquipos(){
		return equipoServiceImpl.listarEquipos();
	}
	
	@PostMapping("/equipos")
	public Equipo guardarEquipoController(@RequestBody Equipo equipo) {	
		return equipoServiceImpl.guardarEquipo(equipo);
	}
	
	@GetMapping("/equipos/{id}")
	public Equipo encontrarEquipoIdController(@PathVariable(name="id") int id){
		Equipo equipoEncontrado = new Equipo();
		equipoEncontrado = equipoServiceImpl.encontrarEquipoId(id);
		return equipoEncontrado;
	}

	@PutMapping("/equipos/{id}")
	public Equipo actualizarDepartamentoController(@PathVariable(name="id") int id, @RequestBody Equipo equipo){
		Equipo equipoSeleccionado = new Equipo();
		Equipo equipoActualizado = new Equipo();
		
		equipoSeleccionado = equipoServiceImpl.encontrarEquipoId(id);
		
		equipoSeleccionado.setNombre(equipo.getNombre());
		equipoSeleccionado.setFacultad(equipo.getFacultad());
		
		equipoActualizado = equipoServiceImpl.actualizarEquipo(equipoSeleccionado);
		
		return equipoActualizado;
	}
	
	@DeleteMapping("/equipos/{id}")
	public void eliminarEquipoController(@PathVariable(name="id") int id){
		equipoServiceImpl.eliminarEquipo(id);
	}
}
