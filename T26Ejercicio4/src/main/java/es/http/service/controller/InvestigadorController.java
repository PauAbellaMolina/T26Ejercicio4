package es.http.service.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.http.service.dto.Investigador;
import es.http.service.service.InvestigadorServiceImpl;

@RestController
@RequestMapping("/api")
public class InvestigadorController {

	@Autowired
	InvestigadorServiceImpl investigadorServiceImpl;
	
	@GetMapping("/investigadores")
	public List<Investigador> listarInvestigadores(){
		return investigadorServiceImpl.listarInvestigadores();
	}
	
	@PostMapping("/investigadores")
	public Investigador guardarInvestigadorController(@RequestBody Investigador investigador) {	
		return investigadorServiceImpl.guardarInvestigador(investigador);
	}
	
	@GetMapping("/investigadores/{id}")
	public Investigador encontrarInvestigadorIdController(@PathVariable(name="id") int id){
		Investigador investigadorEncontrado = new Investigador();
		investigadorEncontrado = investigadorServiceImpl.encontrarInvestigadorId(id);
		return investigadorEncontrado;
	}

	@PutMapping("/investigadores/{id}")
	public Investigador actualizarDepartamentoController(@PathVariable(name="id") int id, @RequestBody Investigador investigador){
		Investigador investigadorSeleccionado = new Investigador();
		Investigador investigadorActualizado = new Investigador();
		
		investigadorSeleccionado = investigadorServiceImpl.encontrarInvestigadorId(id);
		
		investigadorSeleccionado.setNomapels(investigador.getNomapels());
		investigadorSeleccionado.setFacultad(investigador.getFacultad());
		
		investigadorActualizado = investigadorServiceImpl.actualizarInvestigador(investigadorSeleccionado);
		
		return investigadorActualizado;
	}
	
	@DeleteMapping("/investigadors/{id}")
	public void eliminarInvestigadorController(@PathVariable(name="id") int id){
		investigadorServiceImpl.eliminarInvestigador(id);
	}
}
