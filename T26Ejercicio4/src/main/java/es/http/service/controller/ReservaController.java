package es.http.service.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.http.service.dto.Reserva;
import es.http.service.service.ReservaServiceImpl;

@RestController
@RequestMapping("/api")
public class ReservaController {

	@Autowired
	ReservaServiceImpl reservaServiceImpl;
	
	@GetMapping("/reservas")
	public List<Reserva> listarReservas(){
		return reservaServiceImpl.listarReservas();
	}
	
	@PostMapping("/reservas")
	public Reserva guardarReservaController(@RequestBody Reserva reserva) {	
		return reservaServiceImpl.guardarReserva(reserva);
	}
	
	@GetMapping("/reservas/{id}")
	public Reserva encontrarReservaIdController(@PathVariable(name="id") int id){
		Reserva reservaEncontrado = new Reserva();
		reservaEncontrado = reservaServiceImpl.encontrarReservaId(id);
		return reservaEncontrado;
	}

	@PutMapping("/reservas/{id}")
	public Reserva actualizarDepartamentoController(@PathVariable(name="id") int id, @RequestBody Reserva reserva){
		Reserva reservaSeleccionado = new Reserva();
		Reserva reservaActualizado = new Reserva();
		
		reservaSeleccionado = reservaServiceImpl.encontrarReservaId(id);
		
		reservaSeleccionado.setComienzo(reserva.getComienzo());
		reservaSeleccionado.setFin(reserva.getFin());
		reservaSeleccionado.setInvestigador(reserva.getInvestigador());
		reservaSeleccionado.setEquipo(reserva.getEquipo());
		
		reservaActualizado = reservaServiceImpl.actualizarReserva(reservaSeleccionado);
		
		return reservaActualizado;
	}
	
	@DeleteMapping("/reservas/{id}")
	public void eliminarReservaController(@PathVariable(name="id") int id){
		reservaServiceImpl.eliminarReserva(id);
	}
}
