package es.http.service.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.http.service.dto.Facultad;
import es.http.service.service.FacultadServiceImpl;

@RestController
@RequestMapping("/api")
public class FacultadController {

	@Autowired
	FacultadServiceImpl facultadServiceImpl;
	
	@GetMapping("/facultades")
	public List<Facultad> listarFacultades(){
		return facultadServiceImpl.listarFacultades();
	}
	
	@PostMapping("/facultades")
	public Facultad guardarFacultadController(@RequestBody Facultad facultad) {	
		return facultadServiceImpl.guardarFacultad(facultad);
	}
	
	@GetMapping("/facultades/{id}")
	public Facultad encontrarFacultadIdController(@PathVariable(name="id") int id){
		Facultad facultadEncontrado = new Facultad();
		facultadEncontrado = facultadServiceImpl.encontrarFacultadId(id);
		return facultadEncontrado;
	}

	@PutMapping("/facultades/{id}")
	public Facultad actualizarDepartamentoController(@PathVariable(name="id") int id, @RequestBody Facultad facultad){
		Facultad facultadSeleccionado = new Facultad();
		Facultad facultadActualizado = new Facultad();
		
		facultadSeleccionado = facultadServiceImpl.encontrarFacultadId(id);
		
		facultadSeleccionado.setNombre(facultad.getNombre());
		
		facultadActualizado = facultadServiceImpl.actualizarFacultad(facultadSeleccionado);
		
		return facultadActualizado;
	}
	
	@DeleteMapping("/facultades/{id}")
	public void eliminarFacultadController(@PathVariable(name="id") int id){
		facultadServiceImpl.eliminarFacultad(id);
	}
}
