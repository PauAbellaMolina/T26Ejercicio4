package es.http.service.service;

import java.util.List;

import es.http.service.dto.Investigador;

public interface IInvestigadorService {
	
	public List<Investigador> listarInvestigadores();
	
	public Investigador guardarInvestigador(Investigador investigador);

	public Investigador encontrarInvestigadorId(int id);
	
	public Investigador actualizarInvestigador(Investigador investigador);
	
	public void eliminarInvestigador(int id);
}
