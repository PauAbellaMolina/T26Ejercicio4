package es.http.service.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.http.service.dao.IFacultadDAO;
import es.http.service.dto.Facultad;

@Service
public class FacultadServiceImpl implements IFacultadService {

	@Autowired
	IFacultadDAO iFacultadDAO;
	
	@Override
	public List<Facultad> listarFacultades(){
		return iFacultadDAO.findAll();
	}
	
	@Override
	public Facultad guardarFacultad(Facultad facultad) {
		return iFacultadDAO.save(facultad);
	}
	
	@Override
	public Facultad encontrarFacultadId(int id) {
		return iFacultadDAO.findById(id).get();
	}
	
	@Override
	public Facultad actualizarFacultad(Facultad facultad) {
		return iFacultadDAO.save(facultad);
	}
	
	@Override
	public void eliminarFacultad(int id) {
		iFacultadDAO.deleteById(id);
	}
}
