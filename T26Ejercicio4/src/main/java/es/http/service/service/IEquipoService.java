package es.http.service.service;

import java.util.List;

import es.http.service.dto.Equipo;

public interface IEquipoService {
	
	public List<Equipo> listarEquipos();
	
	public Equipo guardarEquipo(Equipo equipo);

	public Equipo encontrarEquipoId(int id);
	
	public Equipo actualizarEquipo(Equipo equipo);
	
	public void eliminarEquipo(int id);
}
