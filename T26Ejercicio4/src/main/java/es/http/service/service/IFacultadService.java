package es.http.service.service;

import java.util.List;

import es.http.service.dto.Facultad;

public interface IFacultadService {
	
	public List<Facultad> listarFacultades();
	
	public Facultad guardarFacultad(Facultad facultad);

	public Facultad encontrarFacultadId(int id);
	
	public Facultad actualizarFacultad(Facultad facultad);
	
	public void eliminarFacultad(int id);
}
