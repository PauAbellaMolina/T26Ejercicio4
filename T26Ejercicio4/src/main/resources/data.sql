DROP TABLE IF EXISTS `reserva`;
DROP TABLE IF EXISTS `equipos`;
DROP TABLE IF EXISTS `investigadores`;
DROP TABLE IF EXISTS `facultad`;

CREATE TABLE `facultad` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) NULL,
  PRIMARY KEY (`id`)
);

insert into facultad (id,nombre)values(1,'Ciencias');
insert into facultad (id,nombre)values(2,'Artes');
insert into facultad (id,nombre)values(3,'Humanidades');

CREATE TABLE `investigadores` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nomapels` VARCHAR(255) NULL,
  `facultad` INT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `ifac_id`
  FOREIGN KEY (`facultad`)
  REFERENCES `facultad` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE
);

insert into investigadores (id,nomapels,facultad)values(1,'Pau Abella',1);
insert into investigadores (id,nomapels,facultad)values(2,'Josep Montero',2);
insert into investigadores (id,nomapels,facultad)values(3,'John Brown',3);

CREATE TABLE `equipos` (
  `id` INT NOT NULL,
  `nombre` VARCHAR(100) NULL,
  `facultad` INT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `efac_id`
  FOREIGN KEY (`facultad`)
  REFERENCES `facultad` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE
);

insert into equipos (id,nombre,facultad)values(1111,'Equip 11AA',1);
insert into equipos (id,nombre,facultad)values(2222,'Equip 22BB',2);
insert into equipos (id,nombre,facultad)values(3333,'Equip 33CC',3);

CREATE TABLE `reserva` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `dni` INT NOT NULL,
  `numserie` INT NOT NULL,
  `comienzo` DATETIME NULL,
  `fin` DATETIME NULL,
  PRIMARY KEY (`id`),
  INDEX `equ_numserie_idx` (`numserie` ASC) VISIBLE,
  INDEX `inv_dni_idx` (`dni` ASC) VISIBLE,
  CONSTRAINT `inv_dni`
    FOREIGN KEY (`dni`)
    REFERENCES `investigadores` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `equ_numserie`
    FOREIGN KEY (`numserie`)
    REFERENCES `equipos` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

insert into reserva (dni,numserie,comienzo,fin)values(1,1111,'2020-09-03 18:44:00','2020-09-03 19:44:00');
insert into reserva (dni,numserie,comienzo,fin)values(2,2222,'2020-09-04 18:00:00','2020-09-05 18:00:00');
insert into reserva (dni,numserie,comienzo,fin)values(3,3333,'2020-09-10 09:00:00','2020-10-01 21:30:00');
